import random
import numpy as np
from math import ceil
import argparse
import sys
from utils import loadtxt

class Graph(object):
    def __init__(self, node_num, edge_file_name, directed, self_ref=False):
        '''
        if self_ref == True:
            self.s_matrix[i, i] = 1
        else:
            self.s_matrix[i, i] = 0
        '''
        cites = loadtxt(edge_file_name)
        edges = np.zeros(cites.shape, np.int32)
        edges[:, 0] = cites[:, 1]
        edges[:, 1] = cites[:, 0]
        self.graph = self.get_graph(edges, node_num, directed)
        s_matrix = np.zeros((node_num, node_num))
        s_matrix[edges[:, 0], edges[:, 1]] = 1
        if not directed:
            s_matrix[edges[:, 1], edges[:, 0]] = 1
        if self_ref:
            s_matrix += np.eye(node_num)
        self.s_matrix = s_matrix

    def get_graph(self, edges, node_num, directed):
        ''' directed: directed graph or not.
            edges(np.ndarray, node_num * 2): edges[i,0] -> edges[i,1]
        '''
        graph = [set() for i in range(node_num)]
        for edge in edges:
            n1 = edge[0]
            n2 = edge[1]
            graph[n1].add(n2)
            if not directed:
                graph[n2].add(n1)
        return graph

    def similarity_matrix(self, step=1):
        '''generate the similarity matrix of the nodes.
            step: the similarity matrix in `step` steps, each row are normalized to one.
        '''
        matrix = np.array(self.s_matrix, dtype=float)
        s_matrix = np.array(self.s_matrix, dtype=float)
        for i in range(1, step):
            matrix = np.dot(matrix, self.s_matrix)
            s_matrix += matrix
        s_matrix = s_matrix / float(step) # follow the TADW setting
        return np.array(s_matrix)


class NegSample(object):
    '''
    every time, sample a batch of [i, c, gamma] from the graph.
    '''
    def __init__(self, graph, path_len, window, negative):
        '''negative: the number of negative nodes for each positive node.'''
        self.graph = graph
        self.path_len = path_len
        self.negative = negative
        self.window = window
        self.nodes = range(len(graph))

    def gen_path(self, nid, path_len):
        '''generate a path which length is path_len start from nid '''
        path = [nid]
        while len(path) < path_len:
            neig = self.graph[path[-1]]
            if len(neig) == 0:
                path = [nid for i in range(path_len)]
                break
            else:
                path.append(random.choice(list(neig)))
        return path

    def gen_paths(self, nodes, path_len):
        paths = np.zeros((len(nodes), path_len), dtype=np.int32) * -1
        for nid in nodes:
            paths[nid, :] = self.gen_path(nid, path_len)
        return paths

    def get_context(self, path, idx):
        ''' get context of node.
        idx: current node idx in path.
        '''
        if len(path) == 1:
            ctx = [(path[idx]), path[idx], 1]
            return ctx
        start = max(0, idx - self.window)
        end = min(len(path), self.window + idx + 1)
        ctx = []
        for pos_idx in range(start, end):
            # if pos_idx != idx: # remove
            ctx.append(path[pos_idx])
        return ctx

    def get_ctx_ids(self, path_len):
        '''
        ctxs[i] is the context indexs of path[i]
        '''
        ctxs = []
        for idx in range(path_len):
            start = max(0, idx - self.window)
            end = min(path_len, self.window + idx + 1)
            ctx = []
            for pos_idx in range(start, end):
                if pos_idx != idx: # remove in 2017-5-19
                    ctx.append(pos_idx)
            ctxs.append(ctx)
        return ctxs

    def negative_sampling(self, pos_samps, power):
        '''
        pos_samps: all of the positive samples
        negative: ratio of negative samples
        power: distribution of sampling.
        '''
        node_num = len(self.nodes)
        p = np.zeros(node_num)
        for i in range(len(self.graph)):
            p[i] = len(self.graph[i])**power # power(degree, power)
        p = p / np.sum(p)
        negs = np.random.choice(self.nodes, size=pos_samps.shape[0] * self.negative, p=p)
        neg_samps = np.zeros((negs.shape[0], 3), np.int32)
        neg_samps[:, 2] = -1
        for i in range(pos_samps.shape[0]):
            neg_samps[self.negative * i: self.negative * (i + 1), 0] = pos_samps[i, 0]
            neg_samps[self.negative * i: self.negative * (i + 1), 1] = negs[self.negative * i: self.negative * (i + 1)]
        return neg_samps

    def get_all_samples(self, power=3.0/4):
        nodes = self.nodes
        np.random.shuffle(nodes)
        ctx_ids = self.get_ctx_ids(self.path_len)
        paths = self.gen_paths(nodes, self.path_len)
        pos_samps = []
        ys = np.ones((len(nodes), 1), dtype=np.int32)
        for idx, ctx in enumerate(ctx_ids):
            ns = paths[:, idx]
            ns = np.reshape(ns, newshape=(ns.shape[0], 1))
            for c in ctx:
                cs = paths[:, c]
                cs = np.reshape(cs, newshape=(cs.shape[0], 1))
                samps = np.concatenate((ns, cs, ys), axis=1)
                pos_samps += samps.tolist()

        pos_samps = np.array(pos_samps, dtype=np.int32)
        neg_samps = self.negative_sampling(pos_samps, power)
        samps = np.concatenate((pos_samps, neg_samps), axis=0)
        np.random.shuffle(samps)
        return samps

    def next_samples(self, batch_size):
        '''batch_size: each time, return a batch of samples'''
        samps = self.get_all_samples()
        batchs = int(ceil(float(samps.shape[0]) / batch_size))
        remaining_batchs = batchs - 1
        for b in range(batchs - 1):
            remaining_batchs -= 1
            yield np.array(samps[b * batch_size: (b + 1) * batch_size], np.int32)

        yield np.array(samps[(batchs - 1) * batch_size: -1], np.int32)


def test_samples():
    follows = {}
    for i in range(5):
        follows[i] = random.sample(range(5), 3)
    for k, v in follows.items():
        print k, v

    sample = NegSample(follows, 3, 1, 2)
    gen = sample.next_sample()
    for i in range(10):
        print '=' * 79
        pos_samps, neg_samps = gen.next()
        print 'pos samps --------'
        print pos_samps
        print 'neg samps --------'
        print neg_samps

