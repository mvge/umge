'''
target-function:
    min L(G) = dw + \alpha * gae + \beta *cae + \gamma * L2
'''
import theano.tensor as T
import theano
import numpy as np
import copy
import lasagne
theano.config.exception_verbosity = 'high'


class Mvge(object):
    def __init__(self,
        embs_size,
        gae,        # auto-encoder for graph similarity matrix(fine-tuning)
        cae,        # auto-encoder for content matrix(fine-tuning)
        lambda_reg,  # hyper-params for regression
        lambda_cae, # hyper-params for auto-encoder part.
        lambda_gae, # hyper-params for auto-encoder part.
        optimizer,
        lr,
        glr=0,
        clr=0,
    ):
        # object:
        #  dw + lambda_cae * cae + lambda_gae * gae + lambda_reg * (reg_g + reg_c)
        self.embs_size = embs_size
        # self.g_matrix = theano.shared(g_matrix, borrow=True)
        # self.c_matrix = theano.shared(c_matrix, borrow=True)
        self.lambda_reg = lambda_reg
        self.lambda_cae = lambda_cae
        self.lambda_gae = lambda_gae
        self.gae = gae
        self.cae = cae

        self.dw_input = T.imatrix(name='dw_input')
        self.lr = lr
        if glr == 0:
            self.glr = lr
        else:
            self.glr = glr
        if clr == 0:
            self.clr = lr
        else:
            self.clr = clr
        self.optimizer = optimizer
        self.build()

    def build(self):
        dw_cost = self.get_dw_cost()
        '''
        object from:
        lambda_dw * dw + \
        lambda_cae * (cae + norm_lambda_c * reg_c) + \
        lambda_gae * (gae + norm_lambda_g * reg_g)
        to:
        dw + lambda_cae * cae + lambda_gae * gae + lambda_reg * (reg_g + reg_c)
        so we have:
        lambda_dw = 1
        norm_lambda_c = lambda_reg / lambda_cae
        norm_lambda_g = lamba_reg / lambda_gae
        '''
        self.cae.norm_lambda = self.lambda_reg / self.lambda_cae
        self.gae.norm_lambda = self.lambda_reg / self.lambda_gae
        c_cost, tmp = self.cae.get_cost_updates()
        c_cost = self.lambda_cae * c_cost
        g_cost, tmp = self.gae.get_cost_updates()
        g_cost = self.lambda_gae * g_cost
        # dw_cost = self.lambda_dw * dw_cost
        all_cost = dw_cost + c_cost + g_cost
        all_params = self.gae.params + self.cae.params

        # update the params in gae
        if self.optimizer == 'sgd':
            gae_updates = lasagne.updates.sgd(all_cost, self.gae.params, self.glr)
            cae_updates = lasagne.updates.sgd(all_cost, self.cae.params, self.clr)
            all_updates = lasagne.updates.sge(all_cost, all_params, self.lr)
        elif self.optimizer == 'adam':
            gae_updates = lasagne.updates.adam(all_cost, self.gae.params, self.glr)
            cae_updates = lasagne.updates.adam(all_cost, self.cae.params, self.clr)
            all_updates = lasagne.updates.adam(all_cost, all_params, self.lr)
        elif self.optimizer == 'adadelta':
            gae_updates = lasagne.updates.adadelta(all_cost, self.gae.params, self.glr)
            cae_updates = lasagne.updates.adadelta(all_cost, self.cae.params, self.clr)
            all_updates = lasagne.updates.adadelta(all_cost, all_params, self.lr)

        gae_train_fn = theano.function(
            [self.dw_input, self.gae.inputs, self.cae.inputs],
            all_cost,
            updates=gae_updates
        )

        cae_train_fn = theano.function(
            [self.dw_input, self.gae.inputs, self.cae.inputs],
            all_cost,
            updates=cae_updates
        )

        all_train_fn = theano.function(
            [self.dw_input, self.gae.inputs, self.cae.inputs],
            all_cost,
            updates=all_updates
        )

        self.cae_cost_func = theano.function([self.cae.inputs], c_cost)
        self.gae_cost_func = theano.function([self.gae.inputs], g_cost)
        self.dw_cost_func = theano.function([self.dw_input], dw_cost)
        self.all_cost_func = theano.function(
            [self.dw_input, self.gae.inputs, self.cae.inputs],
            all_cost
        )

        self.gae_train_fn = gae_train_fn
        self.cae_train_fn = cae_train_fn
        self.all_train_fn = all_train_fn

    def get_dw_cost(self):
        phi_batch = self.gae.in_datas[self.dw_input[:, 0]]
        phi = self.gae.get_hidden_values(phi_batch)

        psi_batch = self.cae.in_datas[self.dw_input[:, 1]]
        psi = self.cae.get_hidden_values(psi_batch)

        L = T.sum(phi * psi, axis=1) * self.dw_input[:, 2]
        L = T.nnet.sigmoid(L)
        L = -T.log(L)
        cost = T.mean(L)
        return cost

    def merge_train(self, samps, gae_inputs, cae_inputs):
        return self.all_train_fn(samps, gae_inputs, cae_inputs)

    def coordinate_train(self, samps, gae_inputs, cae_inputs):
        c = 0
        c = self.gae_train_fn(samps, gae_inputs, cae_inputs)
        c += self.cae_train_fn(samps, gae_inputs, cae_inputs)
        return c

def test_mvge():
    np.random.seed(0)
    g_matrix = np.ones((5, 10))
    c_matrix = np.ones((5, 15))
    input_dw = np.zeros((5 * 4 * 2, 3), int)
    for i in range(5 * 4 * 2):
        input_dw[i] = [i % 5, (i + 1) % 5, np.random.choice([1, 1, 1, -1])]

    mvge1 = Mvge(
        embs_size=5,
        lambda1=0.5,
        corruption_level=0.3,
        g_matrix=g_matrix,
        c_matrix=c_matrix,
        seed=0
    )
    mvge2 = copy.deepcopy(mvge1)

    for i in range(1000):
        cost = mvge1.coordinate_train(input_dw)
        print cost[0], '\t', cost[1], '\t', cost[2], '\t', np.sum(cost)

    for i in range(1000):
        cost = mvge2.train(input_dw)
        print cost

if __name__ == '__main__':
    test_mvge()
