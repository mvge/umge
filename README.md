Usage: 
train:
python mvge_train --config_path mvge_fine_turn.json --log_path run.log --output mvge.embs

test:
python classify_test.py --embs_path mvge.embs --data_dir train_data
