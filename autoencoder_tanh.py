import theano
import theano.tensor as T
import lasagne
import numpy as np
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import utils
from sklearn.linear_model import LogisticRegression as LR
from sklearn.svm import LinearSVC as SVC
from sklearn import preprocessing
import logging

floatX = theano.config.floatX


class dAE(object):
    """Denoising Auto-Encoder class (dA)

    A denoising autoencoders tries to reconstruct the input from a corrupted
    version of it by projecting it first in a latent space and reprojecting
    it afterwards back in the input space. Please refer to Vincent et al.,2008
    for more details. If x is the input then equation (1) computes a partially
    destroyed version of x by means of a stochastic mapping q_D. Equation (2)
    computes the projection of the input into the latent space. Equation (3)
    computes the reconstruction of the input, while equation (4) computes the
    reconstruction error.

    .. math::

        \tilde{x} ~ q_D(\tilde{x}|x)                                     (1)

        y = s(W \tilde{x} + b)                                           (2)

        x = s(W' y  + b')                                                (3)

        L(x,z) = -sum_{k=1}^d [x_k \log z_k + (1-x_k) \log( 1-z_k)]      (4)

    """

    def __init__(
        self,
        numpy_rng,
        theano_rng,
        in_datas,   # original datas, n * m.
        panels,     # panels matrix for original datas.
        n_hidden,
        norm_lambda,
        corruption_level=0.0,
        W=None,
        bhid=None,
        bvis=None,
        optimizer='sgd',
        lr=0.01
    ):
        """
        Initialize the dA class by specifying the number of visible units (the
        dimension d of the input ), the number of hidden units ( the dimension
        d' of the latent or hidden space ) and the corruption level. The
        constructor also receives symbolic variables for the input, weights and
        bias. Such a symbolic variables are useful when, for example the input
        is the result of some computations, or when weights are shared between
        the dA and an MLP layer. When dealing with SdAs this always happens,
        the dA on layer 2 gets as input the output of the dA on layer 1,
        and the weights of the dA are used in the second stage of training
        to construct an MLP.

        :type numpy_rng: numpy.random.RandomState
        :param numpy_rng: number random generator used to generate weights

        :type theano_rng: theano.tensor.shared_randomstreams.RandomStreams
        :param theano_rng: Theano random generator; if None is given one is
                     generated based on a seed drawn from `rng`

        :type n_hidden: int
        :param n_hidden:  number of hidden units

        :type W: theano.tensor.TensorType
        :param W: Theano variable pointing to a set of weights that should be
                  shared belong the dA and another architecture; if dA should
                  be standalone set this to None

        :type bhid: theano.tensor.TensorType
        :param bhid: Theano variable pointing to a set of biases values (for
                     hidden units) that should be shared belong dA and another
                     architecture; if dA should be standalone set this to None

        :type bvis: theano.tensor.TensorType
        :param bvis: Theano variable pointing to a set of biases values (for
                     visible units) that should be shared belong dA and another
                     architecture; if dA should be standalone set this to None
        """
        self.numpy_rng = numpy_rng
        self.theano_rng = theano_rng
        self.in_datas = theano.shared(in_datas, name='inputs', borrow=True)
        self.in_datas = T.cast(self.in_datas, dtype=floatX)
        self.panels = theano.shared(panels, name='panels', borrow=True)
        self.n_visible = in_datas.shape[1]
        self.n_hidden = n_hidden
        self.norm_lambda = norm_lambda
        self.corruption_level = corruption_level
        # self.learning_rate = T.fscalar(name='learning_rate')
        self.learning_rate = lr
        self.inputs = T.ivector(name='inputs')
        self.optimizer = optimizer
        # create a Theano random generator that gives symbolic random values
        if theano_rng is None:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

        # note : W' was written as `W_prime` and b' as `b_prime`
        if W is None:
            initial_W = np.asarray(
                numpy_rng.uniform(
                    low=-4 * np.sqrt(6. / (n_hidden + self.n_visible)),
                    high=4 * np.sqrt(6. / (n_hidden + self.n_visible)),
                    size=(self.n_visible, n_hidden)
                ),
                dtype=theano.config.floatX
            )
            W = theano.shared(value=initial_W, name='W', borrow=True)

        # note : W' was written as `W_prime` and b' as `b_prime`
        W_prime = None
        if W_prime is None:
            W_prime = np.asarray(
                numpy_rng.uniform(
                    low=-4 * np.sqrt(6. / (n_hidden + self.n_visible)),
                    high=4 * np.sqrt(6. / (n_hidden + self.n_visible)),
                    size=(n_hidden, self.n_visible)
                ),
                dtype=theano.config.floatX
            )
            W_prime = theano.shared(value=W_prime, name='W_prime', borrow=True)

        if bvis is None:
            bvis = theano.shared(
                value=np.zeros(
                    self.n_visible,
                    dtype=theano.config.floatX
                ),
                borrow=True
            )
        if bhid is None:
            bhid = theano.shared(
                value=np.zeros(
                    n_hidden,
                    dtype=theano.config.floatX
                ),
                name='b',
                borrow=True
            )

        self.W = W
        # b corresponds to the bias of the hidden
        self.b = bhid
        # b_prime corresponds to the bias of the visible
        self.b_prime = bvis
        # tied weights, therefore W_prime is W transpose
        self.W_prime = self.W.T
        # self.W_prime = W_prime
        self.theano_rng = theano_rng

        self.params = [self.W, self.b, self.b_prime]
        self.get_W_fn = theano.function([], self.W)
        self.get_b_fn = theano.function([], self.b)
        # self.get_W_prime_fn = theano.function([], self.W_prime)
        self.get_b_prime_fn = theano.function([], self.b_prime)
        self.build()

    def get_corrupted_input(self, input):
        """This function keeps ``1-corruption_level`` entries of the inputs the
        same and zero-out randomly selected subset of size ``coruption_level``
        Note : first argument of theano.rng.binomial is the shape(size) of
               random numbers that it should produce
               second argument is the number of trials
               third argument is the probability of success of any trial

                this will produce an array of 0s and 1s where 1 has a
                probability of 1 - ``corruption_level`` and 0 with
                ``corruption_level``

                The binomial function return int64 data type by
                default.  int64 multiplicated by the input
                type(floatX) always return float64.  To keep all data
                in floatX when floatX is float32, we set the dtype of
                the binomial to floatX. As in our case the value of
                the binomial is always 0 or 1, this don't change the
                result. This is needed to allow the gpu to work
                correctly as it only support float32 for now.

        """
        return self.theano_rng.binomial(size=input.shape, n=1,
                                        p=1 - self.corruption_level,
                                        dtype=theano.config.floatX) * input

    def get_hidden_values(self, input):
        """ Computes the values of the hidden layer """
        # return T.nnet.sigmoid(T.dot(input, self.W) + self.b)
        # return T.nnet.relu(T.dot(input, self.W) + self.b)
        return T.tanh(T.dot(input, self.W) + self.b)

    def get_reconstructed_input(self, hidden):
        """Computes the reconstructed input given the values of the
        hidden layer
        """
        # return T.nnet.sigmoid(T.dot(hidden, self.W_prime) + self.b_prime)
        # return T.nnet.relu(T.dot(hidden, self.W_prime) + self.b_prime)
        return T.tanh(T.dot(hidden, self.W_prime) + self.b_prime)

    def get_cost_updates(self):
        """ This function computes the cost and the updates for one trainng
        step of the dA
        """
        batch_datas = self.in_datas[self.inputs]
        batch_panels = self.panels[self.inputs]
        tilde_x = self.get_corrupted_input(batch_datas)
        y = self.get_hidden_values(tilde_x)
        z = self.get_reconstructed_input(y)
        self.embs_func = theano.function([self.inputs], y)
        # note : we sum over the size of a datapoint; if we are using
        #        minibatches, L will be a vector, with one entry per
        #        example in minibatch

        L = 1.0 / 2 * T.sum(T.pow((batch_datas - z) * batch_panels, 2), axis=1)
        # note : L is now a vector, where each element is the
        #        cross-entropy cost of the reconstruction of the
        #        corresponding example of the minibatch. We need to
        #        compute the average of all these to get the cost of
        #        the minibatch
        norm = self.norm_lambda * T.sum(T.pow(self.W, 2))
        cost = T.mean(L) + norm
        self.norm_func = theano.function([], norm)
        # compute the gradients of the cost of the `dA` with respect
        # to its parameters
        # generate the list of updates
        if self.optimizer == 'sgd':
            # gparams = T.grad(cost, self.params)
            update = lasagne.updates.sgd(cost, self.params, self.learning_rate)
            update = lasagne.updates.adam(cost, self.params, self.learning_rate)
        elif self.optimizer == 'adadelta':
            update = lasagne.updates.adadelta(cost, self.params, self.learning_rate)
        else:
           raise ValueError('unrecognize optimizer' + self.optimizer)
        # self.cost = cost
        # self.update = update
        self.cost_func = theano.function([self.inputs], cost)
        return (cost, update)

    def get_train_func(self):
        cost, update = self.get_cost_updates()
        train_func = theano.function(
            # [self.inputs, self.learning_rate],
            [self.inputs],
            cost, updates=update
        )
        return train_func

    def build(self):
        self.train_func = self.get_train_func()

    def train(self, samps):
        return self.train_func(samps)

    def save_params(self, fname):
        np.savetxt(fname + '.W', self.get_W_fn())
        np.savetxt(fname + '.b', self.get_b_fn())
        np.savetxt(fname + '.b_prime', self.get_b_prime_fn())

    def load_params(self, fname):
        W = np.loadtxt(fname + '.W', dtype=floatX)
        b = np.loadtxt(fname + '.b', dtype=floatX)
        b_prime = np.loadtxt(fname + '.b_prime', dtype=floatX)
        self.__init__(
            numpy_rng=self.numpy_rng,
            theano_rng=self.theano_rng,
            in_datas=self.in_datas.eval(),
            panels=self.panels.eval(),
            n_hidden=self.n_hidden,
            norm_lambda=self.norm_lambda,
            W=theano.shared(value=W, name='W', borrow=True),
            bhid=theano.shared(value=b, name='b', borrow=True),
            bvis=theano.shared(value=b_prime, name='b_prime', borrow=True),
        )


def train_ae(
    ae,     # the initialized ae.
    node_num, # total numbers.
    # ada,    # bool: adaptive learning rate or not.
    # lr,     # learning rate
    # min_lr, # min learning rate. used only when ada is true.
    max_iter,
    batch_size,
    train_labels_id, # train_labels, first col is node id while the second col is the label
    test_labels_id, # test_labels, first col is node id while the second col is the label
):
    # if ae.optimizer == 'sgd' and ada:
    #    decay = (lr - min_lr) / max_iter
    # else:
    #     decay = 0

    logging.info('start training...')
    rand_inputs = np.array(range(node_num), np.int32)
    all_inputs = range(node_num)
    batchs = node_num // batch_size
    logging.info('#batchs:' + str(batchs))
    eval_iter = 50
    logging.info('init-performance')
    embs = ae.embs_func(all_inputs)
    embs = preprocessing.normalize(embs, norm='l2', axis=1, copy=False)
    res = utils.embs_test(embs, train_labels_id, test_labels_id, model=LR())
    logging.info('[LR]acc: {:04f}, F1: {:04f}'.format(res[0], res[3]))
    res = utils.embs_test(embs, train_labels_id, test_labels_id, model=SVC(C=5.0))
    logging.info('[SVC]acc: {:04f}, F1: {:04f}'.format(res[0], res[3]))
    for i in range(max_iter):
        np.random.shuffle(rand_inputs)
        for j in range(batchs):
            samps = rand_inputs[j * batch_size: (j + 1) * batch_size]
            # cost = ae.train(samps, lr)
            cost = ae.train(samps)
            norm_cost = ae.norm_func()
        if (i + 1) % eval_iter == 0 or (i + 1) == max_iter:
            logging.info('iter: {}, cost: {:.5f}, '\
                         'ae_norm_cost: {:.5f}'.format(i, float(cost), float(norm_cost)))
            embs = ae.embs_func(all_inputs)
            res = utils.embs_test(embs, train_labels_id,
                                  test_labels_id, model=LR())
            logging.info('[LR]acc: {:04f}, F1: {:04f}'.format(res[0], res[3]))

            embs = preprocessing.normalize(embs, norm='l2', axis=1, copy=False)
            res = utils.embs_test(embs,
                                  train_labels_id,
                                  test_labels_id,
                                  model=SVC(C=5.0))
            logging.info('[SVC]acc: {:04f}, F1: {:04f}'.format(res[0], res[3]))
        # lr -= decay
    return ae
    '''
    logging.info 'final score...'
    embs = ae.embs_func(all_inputs)
    res = utils.embs_test(embs, train_labels_id, test_labels_id)
    logging.info(res[0], res[3])
    '''


class Params(object):
    def __init__(self):
        self.dataset = 'citeseer'
        self.panel = 10
        self.norm_lambda = 1e-5
        self.lr = 1e-1
        self.min_lr = 1e-3
        self.adadelta = True
        self.max_iter = 1000
        self.batch_size = 200
        self.n_hidden = 50
        self.corruption_level = 0.0
        self.load_model = False
        self.model_path = None
        self.train_ratio = 0.5
        self.random_part = 0
        # self.ae_name = 'gae_tanh'
        self.optimizer = 'sgd'
        self.graph_self_ref = False # self circule or not
        self.g_step = 1  # graph similarity step
        self.link_pred = False
        self.feature_path = ''
        self.preprocessing = ''

    def __str__(self):
        s = ''
        s += 'dataset={}\n'.format(self.dataset)
        s += 'panel={}\n'.format(self.panel)
        s += 'norm_lambda={}\n'.format(self.norm_lambda)
        s += 'lr={}\n'.format(self.lr)
        s += 'min_lr={}\n'.format(self.min_lr)
        s += 'adadelta={}\n'.format(self.adadelta)
        s += 'max_iter={}\n'.format(self.max_iter)
        s += 'batch_size={}\n'.format(self.batch_size)
        s += 'n_hidden={}\n'.format(self.n_hidden)
        s += 'corruption_level={}\n'.format(self.corruption_level)
        s += 'load_model={}\n'.format(self.load_model)
        s += 'model_path={}\n'.format(self.model_path)
        s += 'train_ratio={}\n'.format(self.train_ratio)
        s += 'random_part={}\n'.format(self.random_part)
        # s += 'ae_name={}\n'.format(self.ae_name)
        s += 'feature_path{}\n'.format(self.feature_path)
        s += 'graph_self_ref={}\n'.format(self.graph_self_ref)
        s += 'g_step={}\n'.format(self.g_step)
        s += 'link_pred={}\n'.format(self.link_pred)
        s += 'optimizer={}\n'.format(self.optimizer)
        s += 'preprocessing={}\n'.format(self.preprocessing)

        # assert s.count('=') == len(self.__dict__)
        return s
