import numpy as np
from sklearn.linear_model import LogisticRegression as LR
import argparse
from utils import embs_test, get_train_datas, loadtxt
import logging


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--embs_path')
    parser.add_argument('--data_dir')
    args = parser.parse_args()
    random_parts = range(10)
    train_ratios = [i / 10.0 for i in range(1, 11)]

    embs = loadtxt(args.embs_path)
    for tr in train_ratios:
        final_results = [0.0, 0.0]
        args.train_ratio = tr
        for rp in random_parts:
            train_labels, test_labels = get_train_datas(args.data_dir,
                                                        args.train_ratio,
                                                        rp)
            model = LR()
            results = embs_test(embs, train_labels, test_labels, model=model)
            np.set_printoptions(precision=4)
            final_results[0] += results[0]
            final_results[1] += results[3]
        final_results[0] = final_results[0] / len(random_parts)
        final_results[1] = final_results[1] / len(random_parts)
        print('train_ratio: {:.2f}, micro-F1: {:.3f}, macro-F1: {:.3f}'.format(
            tr, final_results[0], final_results[1]))


if __name__ == '__main__':
    main()
