'''
this code is the implement of Unsupervised Multi view Graph Embedding
'''
import Graph

import Mvge
import utils
import numpy as np
from autoencoder_tanh import dAE, train_ae, floatX
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
from sklearn import preprocessing
import logging
import sys
import argparse
import json
import copy
import os
intX = np.int32


class Dataset(object):
    '''init the datas for use.'''
    def __init__(self, config):
        self.rng = np.random.RandomState()
        self.theano_rng = RandomStreams(self.rng.randint(2 ** 30))

        self.c_matrix = utils.loadtxt(config['cae']['feature_path'],
                            normalize='l2')
        self.g_matrix = utils.loadtxt(config['gae']['feature_path'],
                            normalize='l2')
        assert self.c_matrix.shape[0] == self.g_matrix.shape[0]
        self.node_num = int(self.c_matrix.shape[0])


def get_sampler(config, node_num):
    # directory = os.path.join(main_dir,
    #                         'datas', config['dataset'], 'preprocess_datas')
    # edge_file_name = os.path.join(directory,
    #                               config['dataset'] + '.cites.id.txt')
    edge_file_path = config['edge_file_path']
    graph = Graph.Graph(node_num, edge_file_path, False)
    sampler = Graph.NegSample(graph.graph,
                              config['path_len'],
                              config['window'],
                              config['negative'])
    return sampler


def init_gae(config, ds, panels, gae_model_name=None):
    gae = dAE(numpy_rng=ds.rng,
        theano_rng=ds.theano_rng,
        in_datas=ds.g_matrix,
        panels=panels,
        n_hidden=config['embs_size'],
        norm_lambda=config['gae']['norm_lambda']
    )
    if not config['pre_train']:
       return gae

    if gae_model_name is not None:
        logging.info('initializing gae from file')
        gae.load_params(gae_model_name)
    else:
        logging.info('pre-training gae')
        gae = train_ae(ae=gae,
            node_num=ds.node_num,
            ada=config['gae']['adadelta'],
            lr=config['gae']['lr'],
            min_lr=config['gae']['min_lr'],
            max_iter=config['gae']['max_iter'],
            batch_size=config['gae']['batch_size'],
            train_labels_id=ds.train_datas,
            test_labels_id=ds.test_datas
        )
    return gae


def init_cae(config, ds, panels, cae_model_name=None):
    cae = dAE(numpy_rng=ds.rng,
        theano_rng=ds.theano_rng,
        in_datas=ds.c_matrix,
        panels=panels,
        n_hidden=config['embs_size'],
        norm_lambda=config['cae']['norm_lambda']
    )
    if not config['pre_train']:
       return cae

    if cae_model_name is not None:
        logging.info('initializing cae from file')
        cae.load_params(cae_model_name)
    else:
        logging.info('pre-training cae')
        cae = train_ae(ae=cae,
            node_num=ds.node_num,
            ada=config['cae']['adadelta'],
            lr=config['cae']['lr'],
            min_lr=config['cae']['min_lr'],
            max_iter=config['cae']['max_iter'],
            batch_size=config['cae']['batch_size'],
            train_labels_id=ds.train_datas,
            test_labels_id=ds.test_datas
        )
    return cae


def get_mvge(config, ds):
    cpanels = np.ones(ds.c_matrix.shape, dtype=floatX)
    gpanels = np.ones(ds.g_matrix.shape, dtype=floatX)

    gae = init_gae(config, ds, gpanels, None)
    cae = init_cae(config, ds, cpanels, None)
    mvge = Mvge.Mvge(
        embs_size=config['embs_size'],
        gae=gae,
        cae=cae,
        lambda_reg=config['lambda_reg'],
        lambda_cae=config['lambda_cae'],
        lambda_gae=config['lambda_gae'],
        optimizer=config['optimizer'],
        lr=config['lr'],
        glr=config['gae']['lr'],
        clr=config['cae']['lr']
    )
    return mvge


def train_mvge(config, ds, output, directed=False):
    '''
    config: all of the params needed for training a mvge model.
    '''
    json.dump(config,
              open(output + '_config.json', 'w'),
              indent=4,
              sort_keys=True)
    logging.info('initializing mvge models')
    mvge = get_mvge(config, ds)
    logging.info('initializing sampler')
    sampler = get_sampler(config, ds.node_num)
    nodes = range(ds.node_num)
    for i in range(config['max_iter']):
        batchs = -1
        for samps in sampler.next_samples(config['batch_size']):
            batchs += 1
            ae_inputs = list(set(samps[:, 0]))
            gae_inputs = samps[:, 0]
            cae_inputs = samps[:, 1]
            if config['merge_train']:
                cost = float(mvge.all_train_fn(samps, gae_inputs, cae_inputs))
                dw_cost = float(mvge.dw_cost_func(samps))
                gae_cost = float(mvge.gae_cost_func(ae_inputs))
                cae_cost = float(mvge.cae_cost_func(ae_inputs))
            else:
                gae_cost = float(mvge.gae_train_fn(samps,
                                 ae_inputs, ae_inputs))
                cae_cost = float(mvge.cae_train_fn(samps,
                                 ae_inputs, ae_inputs))
                dw_cost = float(mvge.dw_cost_func(samps))
                cost = gae_cost + cae_cost + dw_cost
            if batchs % 50 == 0:
                logging.info('global_step: {}, batch:{}, cost:{:.4f}, dw:{:.4f}, '
                             'gae:{:.4f}, cae:{:.4f}'.format(i, batchs,
                             cost, dw_cost, gae_cost, cae_cost))
        logging.info('global_step: {}, batch:{}, cost:{:.4f}, dw:{:.4f}, '
                     'gae:{:.4f}, cae:{:.4f}'.format(i, batchs,
                     cost, dw_cost, gae_cost, cae_cost))

    gembs = mvge.gae.embs_func(nodes)
    cembs = mvge.cae.embs_func(nodes)
    mvge_embs = np.concatenate((gembs, cembs), axis=1)
    np.savetxt(output, mvge_embs)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config_path', required=True)
    parser.add_argument('--log_path', required=True)
    parser.add_argument('--output')
    args = parser.parse_args()
    log_fname = args.log_path
    format = '%(asctime)s %(levelname)-6s: %(message)s'
    datefmt = '%Y-%m-%d %H:%M:%S'
    logging.basicConfig(filename=log_fname, level=logging.DEBUG,
                        format=format, datefmt=datefmt)
    hdl = logging.StreamHandler(stream=sys.stdout)
    fmt = logging.Formatter(fmt=format, datefmt=datefmt)
    hdl.setFormatter(fmt)
    logging.getLogger().addHandler(hdl)

    config = json.load(open(args.config_path))
    logging.info('config:')
    logging.info(json.dumps(config, sort_keys=True, indent=2))
    np.random.seed(config['seed'])
    ds = Dataset(config)

    train_mvge(config, ds, args.output)

