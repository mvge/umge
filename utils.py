import numpy as np
import sklearn.metrics as metrics
from sklearn.linear_model import LogisticRegression as LR
from sklearn import preprocessing


def loadtxt(path, dtype=float, normalize=None):
    '''
    format:
        for each line, the column number must be the same separate by white space.
    '''
    file = open(path)
    embs = []
    for line in file.readlines():
        embs.append(line.split())
    embs = np.asarray(embs, dtype=dtype)
    if normalize is not None and normalize != '':
        if normalize == 'scale':
            embs = preprocessing.scale(embs, axis=1)
        elif normalize == 'l2':
            embs = preprocessing.normalize(embs, axis=1, norm='l2')
        elif normalize == 'l1':
            embs = preprocessing.normalize(embs, axis=1, norm='l1')
        else:
            raise ValueError('unrecognize preprocessing method: ' + normalize)

    return embs



    def get_graph(self, edges, node_num, directed=False):
        ''' directed: directed graph or not.
            edges(np.ndarray, node_num * 2): edges[i,0] -> edges[i,1]
        '''
        graph = [set() for i in range(node_num)]
        for edge in edges:
            n1 = edge[0]
            n2 = edge[1]
            graph[n1].add(n2)
            if not directed:
                graph[n2].add(n1)

        return graph

    def load_datas(self, fname, dtype=float):
        '''
        for each row, the first index is nid and the values are separate by whitespace.
        return (datas, datas_id): in datas, the nid are discard and responsed by index. in datas_id, it is the original data.
        '''
        datas_id = np.loadtxt(fname, dtype)
        assert set(range(datas_id.shape[0])) == set(datas_id[:, 0])

        shape = (datas_id.shape[0], datas_id.shape[1] - 1)
        idx = np.array(datas_id[:, 0], dtype=int)
        datas = np.zeros(shape, dtype)
        datas[idx, :] = datas_id[:, 1:]
        return (datas, datas_id)


class Params(object):
    # params for training the neural network
    def __init__(self):
        self.embedding_size = 0
        self.path_len = 0
        self.window = 0
        self.neg_samp = 0

        self.lambda1 = 0
        self.lambda2 = 0
        self.lambda3 = 0
        self.max_iter = 0
        self.batch = 0
        self.learning_rate = 0.0
        self.cur_node = False
        self.seed = 0
        self.boosting = False

        self.concate_embs = False
        self.adaptive_learning_rate = True

    def __str__(self):
        s = ''
        s += 'embedding_size=' + str(self.embedding_size) + '\n'
        s += 'path_len=' + str(self.path_len) + '\n'
        s += 'window=' + str(self.window) + '\n'
        s += 'neg_samp=' + str(self.neg_samp) + '\n'
        s += 'lambda1=' + str(self.lambda1) + '\n'
        s += 'lambda2=' + str(self.lambda2) + '\n'
        s += 'lambda3=' + str(self.lambda3) + '\n'
        s += 'max_iter=' + str(self.max_iter) + '\n'
        s += 'batch=' + str(self.batch) + '\n'
        s += 'learning_rate=' + str(self.learning_rate) + '\n'
        s += 'cur_node=' + str(self.cur_node) + '\n'
        s += 'seed=' + str(self.seed) + '\n'
        s += 'boosting=' + str(self.boosting) + '\n'
        s += 'adaptive_learning_rate=' + str(self.adaptive_learning_rate) + '\n'
        s += 'concate_embs=' + str(self.concate_embs)

        return s


def one_hot(vector, cols_num=0):
    assert len(vector.shape) == 1
    if cols_num == 0:
        cols_num = np.max(vector)
    onehot = np.zeros(shape=(vector.shape[0], cols_num + 1), dtype=int)
    onehot[range(vector.shape[0]), vector] = 1
    return onehot


def calc_score(y_true, y_pred, average='macro', labels=None):
    '''
    calc the score of multi-class tast.
    parameters
    ----------
    log_file(log_file object): when log_file is not None, the results will write to this log_file.
    '''
    if average == 'None':
        average = None

    micro_f1 = metrics.f1_score(y_true, y_pred, average='micro', labels=labels)
    pred = metrics.precision_score(y_true, y_pred, average=average, labels=labels)
    recall = metrics.recall_score(y_true, y_pred, average=average, labels=labels)
    f1 = metrics.f1_score(y_true, y_pred, average=average, labels=labels, )

    return micro_f1, pred, recall, f1



def embs_test(node_embs, train_labels_id, test_labels_id, labels=None, model=LR()):
    '''
    train(test)_labels_id(shape=(None * 2)): the first column is nid and the second is label
    if train_score is true, print train_score too.
    model: model to train and test.
    '''
    # train a classifier using the learned embs
    train_idx = train_labels_id[:, 0]
    train_embs = node_embs[train_idx, :]
    train_labels = train_labels_id[:, 1]
    model.fit(train_embs, train_labels)
    test_idx = test_labels_id[:, 0]
    test_embs = node_embs[test_idx, :]
    test_labels = test_labels_id[:, 1]
    y_pred = model.predict(test_embs)
    return calc_score(test_labels, y_pred, labels=labels)



def get_train_datas(data_dir, train_ratio, random_part):
    train_path_gen = data_dir + '/train_data/rand_part_{:d}/'\
        'train_ratio={:.1f}.txt'
    test_path_gen = data_dir + '/test_data/rand_part_{:d}.txt'

    train_fname = train_path_gen.format(random_part, train_ratio)
    test_fname = test_path_gen.format(random_part)
    train_labels_id = np.loadtxt(train_fname, int)
    test_labels_id = np.loadtxt(test_fname, int)

    return (train_labels_id, test_labels_id)


